from tabulate import tabulate
from time import sleep

def print_intro():
        intro = """
        ****************************************
        *                                      *
        *              QUAN LI LOP             *
        *        Author: Tuan va Tuong         *
        *                                      *
        *                                      *
        ****************************************
        1. Danh sach hoc sinh
        2. Them hoc sinh
        3. Xoa hoc sinh
        4. Diem trung binh
        5. Thoat
        """
        print(intro)

def tinh_dtb(danh_sach_lop):
        ten_hoc_sinh = input("Ten hoc sinh: ")
        for hoc_sinh in danh_sach_lop:
            if ten_hoc_sinh == hoc_sinh[0]:
                print(f"Diem trung binh cua hoc sinh {ten_hoc_sinh}: {(hoc_sinh[1] + hoc_sinh[2]) / 2}")

def them_ten_hs(danh_sach_lop):
        hoc_sinh = []
        ten_hoc_sinh = str(input('Ten ban muon them:' ''))
        hoc_sinh.append(ten_hoc_sinh)
        math = float(input('Diem Toan cua nguoi ban muon them:' ''))
        hoc_sinh.append(math)
        english = float(input('Diem Anh cua nguoi ban muon them:' ''))
        hoc_sinh.append(english)
        danh_sach_lop.append(hoc_sinh)
        print(f"Da them hoc sinh {ten_hoc_sinh} vao danh sach lop !")

def xoa_hs(danh_sach_lop):
        ten_hoc_sinh = input("Nguoi ban muon xoa: ")
        for hoc_sinh in danh_sach_lop:
            if ten_hoc_sinh == hoc_sinh[0]:
                danh_sach_lop.remove(hoc_sinh)
                print("Da xoa hoc sinh \"" + ten_hoc_sinh + "\" khoi danh sach lop!")

def main():
                print_intro()
                lop = [["Tran Mai Ngo", 10, 8], ["Ly Quoc Tuan", 6, 1.95], ["Nguyen Minh Phu", 1, 9.56], ["Le Nhan To", 6, 1.24], ["Ho Duc Thinh", 4, 2.77], ["Ha Minh Khoa", 5, 0.98], ["Dinh Thanh Tang", 6, 6.65], ["Lam Dung Khoa", 12, 3.21], ["Ly Ngo Sa", 1, 9.44], ["Dinh Tu Tang", 4, 8.81], ["Phan Thuy Tuan", 5, 6.24]]
                EXIT= False
                while not EXIT:
                print()
                option = int(input("Ban chon: "))
                print()
                if option == 2:
                        them_ten_hs(lop)
                elif option == 3:
                        xoa_hs(lop)
                elif option == 4:
                        tinh_dtb(lop)
                elif option == 1:
                        print(tabulate(lop, headers=['STT', 'Ho va ten', 'Toan', 'Anh'], showindex=True))
                elif option == 5:
                        print("Cam on va hen gap lai!")
                        EXIT = True
                        sleep(3)


if __name__ == "__main__":
    main()
